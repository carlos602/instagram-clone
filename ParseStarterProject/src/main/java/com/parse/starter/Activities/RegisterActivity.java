package com.parse.starter.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.parse.starter.Infastructure.User;
import com.parse.starter.R;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    private EditText userName;
    private EditText password;
    private EditText password2;
    private Button registerButton;
    private View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        userName = (EditText) findViewById(R.id.activity_register_userName);
        password = (EditText) findViewById(R.id.activity_register_password);
        password2 = (EditText) findViewById(R.id.activity_register_password2);
        registerButton = (Button) findViewById(R.id.activity_register_registerButton);
        progressBar = findViewById(R.id.activity_register_progressBar);

        progressBar.setVisibility(View.GONE);
        registerButton.setOnClickListener(this);
    }


    public boolean isEmpty (EditText editText){
        if(editText.getText().toString().trim().length()>0){
            return false;
        } else{
            return true;
        }
    }


    private boolean isMatching(EditText editText1, EditText editText2){
        if(editText1.getText().toString().equals(editText2.getText().toString())){
            return  true;
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View view) {
        if(view == registerButton){
            progressBar.setVisibility(View.VISIBLE);
            boolean validationError = false;
            StringBuilder validationErrorMessage = new StringBuilder("Please");

            if (isEmpty(userName)) {
                validationError = true;
                validationErrorMessage.append("enter a username");
            }

            if (isEmpty(password)) {
                if (validationError) {
                    validationErrorMessage.append(", and ");
                }

                validationError = true;
                validationErrorMessage.append("enter a password");
            }

            if (!isMatching(password, password2)) {
                if (validationError) {
                    validationErrorMessage.append(",and");
                }
                validationError = true;
                validationErrorMessage.append("make sure the passwords are matching");
            }
            validationErrorMessage.append(".");

            if (validationError) {
                Toast.makeText(RegisterActivity.this, validationErrorMessage.toString(), Toast.LENGTH_LONG)
                        .show();
                return;
            }

            final ProgressDialog dialog = new ProgressDialog(RegisterActivity.this);
            dialog.setTitle("Please wait");
            dialog.setMessage("Signing up.....");
            dialog.show();


            User user = new User();
            user.setUsername(userName.getText().toString());
            user.setPassword(password.getText().toString());
            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    progressBar.setVisibility(View.GONE);
                    dialog.dismiss();
                    if (e != null) {
                        Toast.makeText(RegisterActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    } else {

                        Intent intent = new Intent(RegisterActivity.this, DispatchActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }

            });
        }
    }
}


