package com.parse.starter.Activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.starter.R;

public class LoginNarrowActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout relativeLayout;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setContentView(R.layout.activity_login_narrow);

        relativeLayout = (RelativeLayout) findViewById(R.id.activity_login_narrow_layout);
        relativeLayout.setOnClickListener(this);

        textView = (TextView) findViewById(R.id.imMangoText);
        textView.setOnClickListener(this);
    }




    @Override
    public void onClick(View view) {
        if (view == relativeLayout || view == textView){
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        }
    }
}
