/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogOutCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.starter.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class UserListActivity extends BaseActivity implements View.OnClickListener {

    private Button logout;
    private ArrayList<String> usernames;
    private ArrayAdapter arrayAdapter;
    private View progressBar;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      //verifyUser();
      setContentView(R.layout.activity_user_list);
      progressBar = findViewById(R.id.activity_user_progress_bar);
      logout =(Button) findViewById(R.id.logOut_button);
      logout.setOnClickListener(this);
      usernames = new ArrayList<String>();
      setTitle("User List");
      arrayAdapter = new ArrayAdapter<String>(this,
              android.R.layout.simple_list_item_1,usernames);


      final ListView userList = (ListView) findViewById(R.id.activity_user_list_list);

      userList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

              Intent intent = new Intent(getApplicationContext(), UserFeedActivity.class);
              intent.putExtra("username",usernames.get(position));
              startActivity(intent);
          }
      });

      ParseQuery<ParseUser> query = ParseUser.getQuery();

      query.whereNotEqualTo("username",ParseUser.getCurrentUser().getUsername());
      query.addAscendingOrder("username");

      query.findInBackground(new FindCallback<ParseUser>() {
          @Override
          public void done(List<ParseUser> objects, ParseException e) {
              if(e == null){
                  if(objects.size()>0){

                      for(ParseUser user :objects){
                          usernames.add(user.getUsername());

                      }
                      userList.setAdapter(arrayAdapter);
                  }
              }
          }
      });
  }


    @Override
    public void onClick(View view) {
        if(view == logout){
            logout.setText(" ");
            logout.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            logUserOut();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    nextActivity();
                    finish();
                }
            },2000);
        }
    }


    public void logUserOut() {
        ParseUser.getCurrentUser().logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.i("SavedInBackground", "Your Awesome! ");
                } else {
                    Log.i("SavedInBackground", "Wrong,but your still Awesome!");
                    Log.d("Exception: ", e.getMessage());
                }
            }
        });
    }


    public void nextActivity(){
        Intent intent = new Intent(this,DispatchActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_user_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id ==R.id.action_share){

            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i,1);
        }
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode ==1 ){

            Uri selectedImage = data.getData();

            try {
                Bitmap bitmapImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);

                Log.i("AppInfo", "Image Recevied");

                ByteArrayOutputStream stream = new ByteArrayOutputStream();

                bitmapImage.compress(Bitmap.CompressFormat.PNG,100,stream);

                byte[]byteArray = stream.toByteArray();

                ParseFile file = new ParseFile("image.png",byteArray);

                ParseObject object = new ParseObject("images");

                object.put("username",ParseUser.getCurrentUser().getUsername());

                object.put("image", file);

                ParseACL parseACL = new ParseACL();
                parseACL.setPublicReadAccess(true);
                object.setACL(parseACL);

                object.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e == null){
                            Toast.makeText(getApplication().getBaseContext(),"Your imaga has been posted! ",Toast.LENGTH_LONG).show();
                        } else{
                            Toast.makeText(getApplication().getBaseContext(),"Error please try again ",Toast.LENGTH_LONG).show();

                        }
                    }
                });


            } catch(IOException e){
                e.printStackTrace();
            }
        }

    }
}



//...........................................Query Practice  on Parse ....................

  //creates a class
    /*
    ParseObject score = new ParseObject("Score");
    score.put("username","rob");
    score.put("score", 96);
    score.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
       if(e == null){
         Log.i("SaveInBackground", "Successful");
       } else {
         Log.i("SaveInBackground" , "Failed");
         e.printStackTrace();
       }
      }
    });

    //score.saveInBackground
    // score.saveEventually(); use if the user does not have an internet connection



ParseQuery<ParseObject> query = ParseQuery.getQuery("Score");

query.getInBackground("b4CxzzXoHr", new GetCallback<ParseObject>() {
@Override
public void done(ParseObject object, ParseException e) {
        if(e == null){
        object.put("score", "200");
        object.saveInBackground();
        } else {
        Log.i("SaveInBackground","Failed");
        }
        }
        });




    ParseQuery<ParseObject> query = ParseQuery.getQuery("Score");
      query.whereEqualTo("username","rob");

    query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> objects, ParseException e) {

        if(e == null){
          Log.i("findInBackground","Retrieved" + objects.size() + " results");

          for(ParseObject object:objects){
            Log.i("findInBackgroundUser" ,String.valueOf( object.get("score")));
          }
        }


      }
    });



      ParseQuery<ParseObject> query = ParseQuery.getQuery("Score");
      query.whereEqualTo("username","rob");
      query.setLimit(1);

      query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> objects, ParseException e) {
              if(e == null){
                  if(objects.size()>0){
                      objects.get(0).put("score,",250);
                      objects.get(0).saveInBackground();
                  }
              }
          }
      });
*/