package com.parse.starter.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.parse.starter.R;

public class SplashActivity extends BaseActivity {
        @Override
        protected void onCreate(Bundle savedState) {
            super.onCreate(savedState);
            setContentView(R.layout.activity_splash);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    nexActivity();
                    finish();
                }
            }, 3000);
        }

        public void nexActivity(){
            Intent intent = new Intent(this,UserListActivity.class);
            startActivity(intent);
            finish();
        }
    }




