package com.parse.starter.Activities;

import android.content.Intent;
import android.os.Bundle;

import com.parse.ParseUser;
import com.parse.starter.Infastructure.User;


public class DispatchActivity extends BaseActivity {
    private boolean isLoggedin;
    public DispatchActivity(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verifyUser();
    }

    public void verifyUser(){
        ParseUser currentUser = ParseUser.getCurrentUser();

        if (User.getCurrentUser() != null) {
            startActivity(new Intent(this, SplashActivity.class));
            finish();
        } else {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }
}
