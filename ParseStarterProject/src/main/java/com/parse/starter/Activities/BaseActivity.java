package com.parse.starter.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.parse.ParseAnalytics;
import com.parse.starter.Infastructure.InstaApplication;

public class BaseActivity extends AppCompatActivity {
    protected InstaApplication application;

    @Override
    protected void onCreate(Bundle savedState){
        super.onCreate(savedState);
        application = (InstaApplication) getApplication();
        ParseAnalytics.trackAppOpenedInBackground(getIntent());

    }

    public InstaApplication getInstaApplication(){
        return  application;
    }
}
