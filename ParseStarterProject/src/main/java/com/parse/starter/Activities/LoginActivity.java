package com.parse.starter.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.parse.starter.Infastructure.User;
import com.parse.starter.R;

public class LoginActivity extends BaseActivity implements View.OnClickListener{
    private static final int REQUEST_NARROW_LOGIN = 1;
    private static final int REQUEST_REGISTER = 2;
    private View loginButton;
    private View registerButton;

    @Override
    protected void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setContentView(R.layout.activity_login);

        loginButton = findViewById(R.id.Log_in);
        registerButton = findViewById(R.id.register_button);

        if(loginButton!= null) {
            loginButton.setOnClickListener(this);
        }

        registerButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view == loginButton){
            startActivityForResult(new Intent(this, LoginNarrowActivity.class), REQUEST_NARROW_LOGIN);

        } else if (view == registerButton){
            startActivityForResult(new Intent(this,RegisterActivity.class),REQUEST_REGISTER);
        }
    }

}
