package com.parse.starter.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.starter.Activities.DispatchActivity;
import com.parse.starter.R;



public class LoginFragment extends Fragment implements View.OnClickListener, View.OnKeyListener {
    private Button loginButton;
    private EditText usernameText;
    private EditText passwordTextview;
    private View progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_login, container, false);


        loginButton = (Button) fragmentView.findViewById(R.id.fragment_loginButton);
        loginButton.setOnClickListener(this);


        progressBar = fragmentView.findViewById(R.id.fragment_login_progress);
        usernameText = (EditText) fragmentView.findViewById(R.id.fragment_login_userName);
        usernameText.setOnKeyListener(this);
        passwordTextview = (EditText) fragmentView.findViewById(R.id.fragment_login_password);
        passwordTextview.setOnKeyListener(this);
        progressBar.setVisibility(View.GONE);
        return fragmentView;
    }


    @Override
    public boolean onKey(View view, int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN){
            LogIn(view);
        }

        return false;
    }

    @Override
    public void onClick(View view) {
        if (view == loginButton) {
            LogIn(view);
        }
    }



    public void LogIn(View view){
        ParseUser.logInInBackground(usernameText.getText().toString(), passwordTextview.getText().toString(),
                new LogInCallback() {
                    @Override
                    public void done(ParseUser user, ParseException e) {
                        if (e != null) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            user.getCurrentUser();
                            user.put("setIsLoggedIn", true);
                            user.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        Log.i("SavedInBackground", "Your Awesome! ");
                                    } else {
                                        Log.i("SavedInBackground", "Wrong,but your still Awesome!");
                                        Log.d("Exception: ", e.getMessage());
                                    }
                                }
                            });
                            Intent intent = new Intent(getActivity(), DispatchActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                });
    }
}

