package com.parse.starter.Infastructure;

import com.parse.ParseClassName;
import com.parse.ParseUser;


@ParseClassName("User")
public class User extends ParseUser {
    public User(){
    }


    private int avatarUrl;
    private boolean isLoggedIn;
    private boolean hasPassword;
    private String email;
    private String Auth;

    public int getAvatarUrl() {

        return avatarUrl;
    }

    public void setAvatarUrl(int avatarUrl) {
        this.avatarUrl = avatarUrl;

    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public boolean isHasPassword() {
        return hasPassword;
    }

    public void setHasPassword(boolean hasPassword) {
        this.hasPassword = hasPassword;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuth() {
        return Auth;
    }

    public void setAuth(String auth) {
        Auth = auth;
    }
}
